﻿using UnityEngine;

public class SaveState
{
    public int currentLevel = 1;

    public int triesCount = 0;
    public int highscore = 0;
}
