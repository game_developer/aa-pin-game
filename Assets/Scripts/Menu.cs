﻿using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour 
{
    public Text highscoreTextValue;
    public Text triesTextValue;

    CampaignLevel nextLevel;

    private void Start()
    {
        int nextLevel = SaveManager.Instance.state.currentLevel;
        this.nextLevel = Resources.Load<CampaignLevel>(@"Level\Level" + nextLevel) as CampaignLevel;

        SetTriesHighscore();
    }

    public void OnCampaignClicked()
    {
        GameData.Instance.state = GameData.GameState.Campaign;
        GameData.Instance.level = nextLevel;

        LoadGame();
    }

    public void OnEndlessClicked()
    {
        GameData.Instance.state = GameData.GameState.Endless;

        LoadGame();
    }

    private void LoadGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
    }

    private void SetTriesHighscore()
    {
        highscoreTextValue.text = SaveManager.Instance.state.highscore.ToString();
        triesTextValue.text = SaveManager.Instance.state.triesCount.ToString();
    }
}
