﻿using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour 
{
    public GameObject pauseMenu;
    private bool isPaused = false;

    private void Start()
    {
        Resume();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ChangeMenuState();
        }
    }

    public void ChangeMenuState()
    {
        if (isPaused)
        {
            Resume();
        }
        else
        {
            Pause();
        }
    }

    public void Resume()
    {
        isPaused = false;
        pauseMenu.SetActive(false);

        Time.timeScale = 1;
    }

    public void Pause()
    {
        isPaused = true;
        pauseMenu.SetActive(true);

        Time.timeScale = 0;
    }

    public void Exit()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
    }
}
