﻿using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public static SaveManager Instance { get; set; }

    public SaveState state;
    private const string SAVE_KEY = "Save";

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        else
        {
            Destroy(gameObject);
        }
        Load();
    }

    public void Save()
    {
        PlayerPrefs.SetString(SAVE_KEY, Helper.Encrypt(Helper.Serialize<SaveState>(state)));
    }

    public void Load()
    {
        if (PlayerPrefs.HasKey(SAVE_KEY))
        {
            state = Helper.Deserialize<SaveState>(Helper.Decrypt( PlayerPrefs.GetString(SAVE_KEY)));
        }
        else
        {
            state = new SaveState();
            Save();
            Debug.Log("Creating new save file");
        }
    }

    public void ResetSave()
    {
        PlayerPrefs.DeleteKey(SAVE_KEY);
    }
}
