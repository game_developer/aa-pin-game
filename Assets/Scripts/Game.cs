﻿using System.Collections;
using UnityEngine;

public class Game : MonoBehaviour
{
    public PinManager pinManager;

    private void Start()
    {
        switch (GameData.Instance.state)
        {
            case GameData.GameState.Campaign:
                SetCampaignMode();
                break;
            case GameData.GameState.Endless:
                SetEndlessMode();
                break;
            default:
                throw new UnityException("State does not implemented");
        }

        PinManager.Victory += OnLevelComplete;
        Pin.Lose += OnPlayerDied;
    }

    private void SetCampaignMode()
    {
        int pinCount = GameData.Instance.level.pinCount;
        Base.rotationSpeed = GameData.Instance.level.baseRotationSpeed;

        pinManager.CreatePins(pinCount);
    }

    private void SetEndlessMode()
    {
        //int pinCount = PinManager.PinCount;
        int pinCount = 60;

        pinManager.CreatePins(pinCount);
        PinManager.EndlessMode = true;
    }

    private void OnLevelComplete()
    {
        SaveManager.Instance.state.currentLevel++;
        SaveManager.Instance.Save();

        PinManager.Victory -= OnLevelComplete;

        LoadMenu();
    }

    private void OnPlayerDied()
    {
        StartCoroutine(TransitionToMenu());

        if (GameData.Instance.state == GameData.GameState.Endless)
        {
            int score = FindObjectOfType<Base>().transform.childCount;
            if (score > SaveManager.Instance.state.highscore)
            {
                SaveManager.Instance.state.highscore = score;
            }

            SaveManager.Instance.state.triesCount++;
        }
    }

    IEnumerator TransitionToMenu()
    {
        FindObjectOfType<Camera>().GetComponent<CameraShake>().enabled = true;

        yield return new WaitForSeconds(1f / 3f);

        SaveManager.Instance.Save();
        LoadMenu();
    }

    private void LoadMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
    }
}
