﻿using UnityEngine;

public class Base : MonoBehaviour 
{
    public static float rotationSpeed = 156f;

    private void FixedUpdate()
    {
        transform.Rotate(Vector3.forward * rotationSpeed * Time.deltaTime);
    }
}
