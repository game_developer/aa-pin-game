﻿using UnityEngine;

public class GameData : MonoBehaviour
{
    public static GameData Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        else
        {
            Destroy(gameObject);
        }

    }

    public GameState state;
    public CampaignLevel level = null;


    public enum GameState
    {
        Campaign,
        Endless
    }
}
