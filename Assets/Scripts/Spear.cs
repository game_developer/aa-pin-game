﻿using UnityEngine;

public class Spear : MonoBehaviour 
{
    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Base")
        {
            transform.parent.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            transform.parent.GetComponent<Pin>().enabled = false;
            GetComponent<BoxCollider2D>().enabled = false;
            GetComponent<Spear>().enabled = false;

            spriteRenderer.enabled = true;

            transform.parent.SetParent(collision.transform);
        }
    }
}
