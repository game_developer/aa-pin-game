﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
public class Pin : MonoBehaviour 
{
    [SerializeField] private float launchStrength = 10f;
    [SerializeField] private GameObject spear;
    private Rigidbody2D rb;

    private static bool hasTriggeredPinCollision = false;
    private bool _wasLaunched = false;
    private bool _launch = false;
    private bool _isFirstInQueue = false;

    public bool IsFirstInQueue
    {
        get
        {
            return _isFirstInQueue;
        }

        set
        {
            if (value == true)
            {
                spear.SetActive(true);
            }
            _isFirstInQueue = value;
        }
    }

    private void Awake()
    {
        spear.SetActive(false);
    }

    #region Lose Event
    public static event Action Lose;

    protected virtual void OnLose()
    {
        if (Lose != null)
        {
            Lose();
        }
    }
    #endregion

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && !_wasLaunched && _isFirstInQueue && !Helper.IsPointerOverUIObject())
        {
            _wasLaunched = true;
            _launch = true;
        }
    }

    private void FixedUpdate()
    {
        if (_launch)
        {
            rb.velocity = Vector2.up * launchStrength * Time.deltaTime;
            _launch = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Pin" && !hasTriggeredPinCollision)
        {
            hasTriggeredPinCollision = true;
            OnLose();
        }
    }
}
