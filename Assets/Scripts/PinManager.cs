﻿using System;
using UnityEngine;

public class PinManager : MonoBehaviour
{
    [SerializeField] private readonly float startPinPosY = 2.5f;
    [SerializeField] private readonly float yPinDistance = 0.25f;

    [HideInInspector] public static bool EndlessMode = false;

    [SerializeField] private Pin pin;

    private int previousChildCount;
    private float lastYPinPos = 2.5f;
    
    #region Victory Event
    public static event Action Victory;

    protected virtual void OnVictory()
    {
        if (Victory != null)
        {
            Victory();
        }
    }
    #endregion
    
    private void Arrange()
    {
        float previousPinY = startPinPosY;

        foreach (Transform pin in transform)
        {
            pin.transform.position = Vector2.down * previousPinY;

            previousPinY += yPinDistance;
        }

        lastYPinPos = previousPinY;
    }

    public void CreatePins(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            float yPos = lastYPinPos;

            if (i != 0)
            {
                yPos += yPinDistance;
            }

            Instantiate(pin, Vector2.down * yPos, Quaternion.identity, transform);
            lastYPinPos = yPos;
        }
    }

    private void OnTransformChildrenChanged()
    {
        int childCount = transform.childCount;

        if (childCount > 0)
        {
            transform.GetChild(0).GetComponent<Pin>().IsFirstInQueue = true;
            if (childCount < previousChildCount)
            {
                Arrange();
            }
        }

        else
        {
            OnVictory();
        }

        previousChildCount = transform.childCount;
    }
}
