﻿using UnityEngine;

[CreateAssetMenu]
public class CampaignLevel : ScriptableObject 
{
    public int pinCount;
    public float baseRotationSpeed;
}
